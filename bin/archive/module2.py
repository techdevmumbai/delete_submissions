import os,re, time, datetime
path = r'T:\XXX\_library\submissions'


ignore_folder_list = ['Error_Reports','Review_Reports','Submission_Lists_Records']


VALID_SUBFOLDERS_IN_SUBMISSION= []
FOLDERS_TO_DELETE = []

root = r'T:\\'

proj_list =  os.listdir(root)


def generate_subfolder_list(submission_path):
    sub_folderList = os.listdir(submission_path)
    for i in sub_folderList:
##        print i
        folder_path =  os.path.join(submission_path,i)
        if os.path.isdir(folder_path):
            if not i.startswith('_'):
                if not i in ignore_folder_list:
                    VALID_SUBFOLDERS_IN_SUBMISSION.append(folder_path)


for i in proj_list:
    if len(i)== 3:
##        print i
        sub_path = re.sub('XXX',i,path)
        if os.path.exists(sub_path):
##            print sub_path
            generate_subfolder_list(sub_path)


for i in VALID_SUBFOLDERS_IN_SUBMISSION:
    now = os.path.getmtime(i)
    gmtime =  time.gmtime(now)
    last_mod =  time.strftime("%Y%m%d", gmtime)
    buffer_month= (datetime.date.today() + datetime.timedelta(-1*365/12)).strftime('%Y%m%d')
    if last_mod<buffer_month:
        print i
        print last_mod
##    print time.strftime("%y/%m/%d %H:%M", now)
##    print "last modified: %s" % time.strftime("%y/%m/%d %H:%M", (os.path.getmtime(i)))
##    print "created: %s" % time.ctime(os.path.getctime(i))