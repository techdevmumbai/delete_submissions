# script to delete redundant data from _turnovers folder from any show.

import re
import datetime
from subprocess import Popen, PIPE
import pf_deadline
import primelog
import time
import os

VALID_SUBFOLDERS_IN_SUBMISSION = []
FOLDERS_TO_DELETE = []
ignore_folder_list = ['Error_Reports','Review_Reports','Submission_Lists_Records']
dummy_submission_path = r'T:\XXX\_library\submissions'
project_name_input = raw_input('Pleas enter project name :')


while not os.path.exists(r't:\\'+project_name_input):
    print'Please enter valid project code'
    project_name_input = raw_input('Please enter project name :')
    submission_path = re.sub('XXX', project_name_input, dummy_submission_path)


submission_path = re.sub('XXX', project_name_input, dummy_submission_path)
##print submission_path

BATFILE = r'T:\_library\viewd_pipeline\SwapnilK\Python\_delete_submissions_data\bin\delete_bat.bat '  # to delete folders from cmd
TASK_LIST = []  # This will contain list of commands
JOB_ID = ''  # this will store current job Id.
DEADLINECOMMAND = "C:\\Program Files\\Thinkbox\\Deadline6\\bin\\deadlinecommand.exe -getjob"

# this loop is to get user input for project name
if not os.path.exists(submission_path):
    print'Submissions folder does no exist.'

report_log = primelog.PrimeLog('delete_submissions', stdout=True, level='INFO')
log = report_log.get_logger()


# Get folder list from _turnover folder and create a text file for next step
def generate_subfolder_list(submission_path):
    sub_folderList = os.listdir(submission_path)
    for i in sub_folderList:
        folder_path =  os.path.join(submission_path,i)
        if os.path.isdir(folder_path):
            if not i.startswith('_'):
                if not i in ignore_folder_list:
                    VALID_SUBFOLDERS_IN_SUBMISSION.append(folder_path)

generate_subfolder_list(submission_path)

def filter_valid_folders():
    for folderName in VALID_SUBFOLDERS_IN_SUBMISSION:
        now = os.path.getmtime(folderName)
        gmtime =  time.gmtime(now)
        last_mod =  time.strftime("%Y%m%d", gmtime)
        buffer_month= (datetime.date.today() + datetime.timedelta(-1*365/12)).strftime('%Y%m%d')
        if last_mod<buffer_month:
            FOLDERS_TO_DELETE.append(folderName)
            log.info('Valid folder name : ' + folderName)
            log.info('Date Created : ' + last_mod)
            TASK_LIST.append(BATFILE + folderName)


filter_valid_folders()
##print TASK_LIST
##print FOLDERS_TO_DELETE

def push_to_deadline(task_name):
    job_name = ' delete_turnover_Task'
    dead = pf_deadline.CommandScript(name=job_name, tasks=task_name, priority=45, machine_limit=02)
    dead.submit()
    if dead:
        job_id = dead.job_id
        log.info('Deadline job id : ' + job_id)
    return job_id

if len(TASK_LIST) > 0:
##    print TASK_LIST
    deadline_job_id = push_to_deadline(TASK_LIST)
    print deadline_job_id

    # Function to check/create a log of Deadline task status.
    def check_status():
        current_stat = "Status=Active"
        while current_stat == "Status=Active":
            deadline_command = r"C:\Program Files\Thinkbox\Deadline6\bin\deadlinecommand.exe"
            command = deadline_command + ' -getjob ' + deadline_job_id
            commandline_process = Popen(command, stdout=PIPE, stderr=PIPE)
            std_out, std_error = commandline_process.communicate()
            job_status = std_out.split("\r\n")
            for status in job_status:
                if re.match('Status', status):
                    print status
                    current_stat = status
            time.sleep(5)
        if current_stat == 'Status=Completed':
            log.info('Job Completed : ' + deadline_job_id)
            print current_stat

#enable entire chunk below this.
    check_status()
else:
    log.error('Task list is empty, could not submit the task to Deadline')
