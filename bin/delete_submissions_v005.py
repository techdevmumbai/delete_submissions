# script to delete redundant data from _turnovers folder from any show.

import re
import datetime
from subprocess import Popen, PIPE
import pf_deadline
import primelog
import time
import os

IGNORE_FOLDER_LIST = ['Error_Reports', 'Review_Reports', 'Submission_Lists_Records']
DUMMY_SUBMISSION_PATH = r'T:\XXX\_library\submissions'
PROJECT_name_input = raw_input('Pleas enter project name :')

while not os.path.exists(r't:\\' + PROJECT_name_input):
    print'Please enter valid project code'
    PROJECT_name_input = raw_input('Please enter project name :')
    submission_path = re.sub('XXX', PROJECT_name_input, DUMMY_SUBMISSION_PATH)

submission_path = re.sub('XXX', PROJECT_name_input, DUMMY_SUBMISSION_PATH)

# this loop is to get user input for project name
if not os.path.exists(submission_path):
    print'Submissions folder does no exist.'

report_log = primelog.PrimeLog('delete_submissions', stdout=True, level='INFO')
log = report_log.get_logger()


# Get folder list from _turnover folder and create a text file for next step
def generate_subfolder_list(submission_path):
    final_subfolder_list = []
    sub_folderList = os.listdir(submission_path)
    for i in sub_folderList:
        folder_path = os.path.join(submission_path, i)
        if os.path.isdir(folder_path):
            if not i.startswith('_') and not i in IGNORE_FOLDER_LIST:
                final_subfolder_list.append(folder_path)
    return final_subfolder_list


def filter_valid_folders(subfolder_list):
    task_list = []
    bat_file = os.path.join(os.path.dirname(__file__), 'delete_bat.bat')
    for folderName in subfolder_list:
        now = os.path.getmtime(folderName)
        gmtime = time.gmtime(now)
        last_mod = time.strftime("%Y%m%d", gmtime)
        buffer_month = (datetime.date.today() + datetime.timedelta(-1 * 365 / 12)).strftime('%Y%m%d')
        if last_mod < buffer_month:
            log.info('Valid folder name : ' + folderName)
            log.info('Date Created : ' + last_mod)
            task_list.append(bat_file + folderName)

    return task_list


def push_to_deadline(task_name):
    job_name = ' delete_turnover_Task'
    dead = pf_deadline.CommandScript(name=job_name, tasks=task_name, priority=45, machine_limit=02)
    dead.submit()
    if dead:
        job_id = dead.job_id
        log.info('Deadline job id : ' + job_id)
    return job_id


# Function to check/create a log of Deadline task status.
def check_status(deadline_job_id):
    current_stat = "Status=Active"
    while current_stat == "Status=Active":
        deadline_command = r"C:\Program Files\Thinkbox\Deadline6\bin\deadlinecommand.exe"
        command = deadline_command + ' -getjob ' + deadline_job_id
        commandline_process = Popen(command, stdout=PIPE, stderr=PIPE)
        std_out, std_error = commandline_process.communicate()
        job_status = std_out.split("\r\n")
        for status in job_status:
            if re.match('Status', status):
                print status
                current_stat = status
        time.sleep(5)
    if current_stat == 'Status=Completed':
        log.info('Job Completed : ' + deadline_job_id)
        print current_stat


def main():
    subfolder_list = generate_subfolder_list(submission_path)
    task_list = filter_valid_folders(subfolder_list)

    if len(task_list) > 0:
        deadline_job_id = push_to_deadline(task_list)
        print deadline_job_id

        check_status(deadline_job_id)
    else:
        log.error('Task list is empty, could not submit anything to Deadline')


main()
